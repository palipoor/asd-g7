import django_heroku
import os

"""
Django settings for Fasd project.

Generated by 'django-admin startproject' using Django 2.1.4.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import pkg_resources as resources


def get_resource_address(resource_name, resource_package):
	"""
	Get absolute address for `resource_name`
	:param str resource_name:
	:param str resource_package:
	:return: absolute path for accessing the resource file inside resources package
	"""
	filepath = resources.resource_filename(resource_package, resource_name)
	return filepath


import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@hke#wuhwlu5)&-1-5wcpk9tamn6b(u5_le(9+8bcg&7_)ga5s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'whitenoise.runserver_nostatic',
	'django.contrib.staticfiles',
	"django_apscheduler",
	'taggit',
	'django_jalali',
	'apps.dashboard',
	'apps.landing',
	'apps.product',
	'apps.registeration',
	'apps.location',
	'apps.messaging',
	'apps.wishlist',
	'apps.notification',
	'apps.shopping',
	'apps.auction',
	'apps.advertisement',

]

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'whitenoise.middleware.WhiteNoiseMiddleware',
]

ROOT_URLCONF = 'backend_settings.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [
			os.path.join(BASE_DIR, 'apps', 'dashboard', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'product', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'landing', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'registeration', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'shopping', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'notification', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'wishlist', 'templates'),
			os.path.join(BASE_DIR, 'apps', 'messaging', 'templates'),

			os.path.join(BASE_DIR, 'templates'),
		],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
				'apps.context_processor.notifs',
			],
		},
	},
]

WSGI_APPLICATION = 'backend_settings.wsgi.application'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql',
		'NAME': 'asdproject',
		'USER': 'asd',
		'PASSWORD': 'asdasd',
		'HOST': 'localhost',
		'PORT': '5432',
	}
}

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
	{
		'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
	},
	{
		'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
	},
]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'fa-IR'

TIME_ZONE = 'Asia/Tehran'
USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/staticfiles/'
STATIC_ROOT = "staticfiles"
STATICFILES_DIRS = [
	os.path.join(BASE_DIR, 'front'),
]
LOGIN_REDIRECT_URL = '/login_success'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587

EMAIL_HOST_USER = 'sakhoufcompani@gmail.com'
EMAIL_HOST_PASSWORD = 'sakhoufsucks'
LOGIN_URL = '/login/'

if DEBUG:
	MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
else:
	MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
# This scheduler config will:
# - Store jobs in the project database
# - Execute jobs in threads inside the application process
SCHEDULER_CONFIG = {
	"apscheduler.jobstores.default": {
		"class": "django_apscheduler.jobstores:DjangoJobStore"
	},
	'apscheduler.executors.processpool': {
		"type": "threadpool"
	},
}
SCHEDULER_AUTOSTART = True
django_heroku.settings(locals())
