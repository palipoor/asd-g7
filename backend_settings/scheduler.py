import logging
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.executors.pool import ProcessPoolExecutor, ThreadPoolExecutor
from django_apscheduler.jobstores import register_events, register_job
from django.apps import apps
from django.conf import settings
from datetime import datetime
from django_apscheduler.jobstores import DjangoJobStore
from dateutil import relativedelta

# Create schedauler to run in a thread inside the application process
from jdatetime import timedelta


def finish_auction(id):
	auction = apps.get_model("auction", "Auction").objects.get(id=id)
	auction.finish()
	auction.save()


def check_auction_pay(id):
	auction = apps.get_model("auction", "Auction").objects.get(id=id)
	if auction.status == "در انتظار پرداخت":
		auction.buyer_decline()
		auction.save()


def check_auction_send(id):
	auction = apps.get_model("auction", "Auction").objects.get(id=id)
	if auction.status == "در انتظار فرستادن":
		auction.seller_decline()
		auction.save()


def finish_advertise(id):
	advertise = apps.get_model("advertisement", "Advertisement").objects.get(id=id)
	advertise.is_active = False
	advertise.save()
	print("finished advertise")

def start_advertise(id):
	advertise = apps.get_model("advertisement", "Advertisement").objects.get(id=id)
	advertise.is_active = True
	advertise.save()
	print("start advertise")

class DjangoScheduler():
	def __init__(self, *args, **kwargs):
		# self.scheduler = BackgroundScheduler(settings.SCHEDULER_CONFIG)
		self.scheduler = BackgroundScheduler()
		self.scheduler.add_jobstore(DjangoJobStore(), "default")

	def add_auction_finish(self, auction):
		print(type(auction.finish_date))
		print(auction.finish_date)
		print(auction)
		job_id = self.scheduler.add_job(finish_auction, 'date', run_date=auction.finish_date.togregorian(),
										args=[auction.id], id="auction_" + str(auction.id), replace_existing=True)
		print("ID")
		print(job_id)
		register_job(job_id)

	def add_auction_send(self, auction):
		print(type(auction.send_date))
		print(auction.send_date)
		print(auction)
		job_id = self.scheduler.add_job(check_auction_send, 'date', run_date=auction.send_date.togregorian(),
										args=[auction.id], id="send_" + str(auction.id), replace_existing=True)
		print("ID")
		print(job_id)
		register_job(job_id)

	def add_auction_pay(self, auction):
		print(type(auction.pay_date))
		print(auction.pay_date)
		print(auction)
		job_id = self.scheduler.add_job(check_auction_pay, 'date', run_date=auction.pay_date.togregorian(),
										args=[auction.id], id="pay_" + str(auction.id), replace_existing=True)
		print("ID")
		print(job_id)
		register_job(job_id)

	def add_end_advertise(self, advertise):
		print(type(advertise.end_date))
		print(advertise.end_date)
		print(advertise)
		job_id = self.scheduler.add_job(finish_advertise, 'date', run_date=advertise.end_date.togregorian(),
										args=[advertise.id], id="finish_advertise_" + str(advertise.id),
										replace_existing=True)
		print("ID")
		print(job_id)
		register_job(job_id)

	def add_start_advertise(self, advertise):
		print(type(advertise.date))
		print(advertise.date)
		print(advertise)
		job_id = self.scheduler.add_job(start_advertise, 'date', run_date=advertise.date.togregorian(),
										args=[advertise.id], id="start_advertise_" + str(advertise.id),
										replace_existing=True)
		print("ID")
		print(job_id)
		register_job(job_id)

	def start(self):
		logging.basicConfig(filename="log")
		logging.getLogger('apscheduler').setLevel(logging.DEBUG)
		if settings.DEBUG:
			register_events(self.scheduler)
		print("hello")
		self.scheduler.start()


project_scheduler = DjangoScheduler()
