from django.conf import settings
from django.conf.urls.static import static

"""asd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
import django.views.static

admin.site.site_header = "سخوف"
urlpatterns = [

	url(r'^media/(?P<path>.*)$', django.views.static.serve,
		{'document_root': settings.MEDIA_ROOT, 'show_indexes': settings.DEBUG}),
	path('admin/', admin.site.urls),
	path('', include(('apps.dashboard.urls', 'dashboard'), 'dashboard')),
	path('', include(('apps.product.urls', 'product'), 'product')),
	path('', include(('apps.landing.urls', 'landing'), 'landing')),
	path('', include(('apps.registeration.urls', 'registeration'), 'registeration')),
	path('', include(('apps.messaging.urls', 'messaging'), 'messaging')),
	path('', include(('apps.wishlist.urls', 'wishlist'), 'wishlist')),
	path('', include(('apps.shopping.urls', 'shopping'), 'shopping')),
	path('', include(('apps.notification.urls', 'notification'), 'notification')),
	path('', include(('apps.location.urls', 'location'), 'location')),
	path('', include(('apps.auction.urls', 'auction'), 'auction')),
	path('', include(('apps.advertisement.urls', 'advertisement'), 'advertisement')),

]
