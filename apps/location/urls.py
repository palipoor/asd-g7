from django.urls import path

from apps.location.views import get_cities, get_districts

urlpatterns = [
	path('helper/get_cities/<int:state_id>', get_cities, name='get_cities'),
	path('helper/get_districts/<int:city_id>', get_districts, name='get_districts'),

]
