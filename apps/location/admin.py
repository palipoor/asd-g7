from django.contrib import admin

# Register your models here.
from apps.location.models import City, State, District

admin.site.register(City)
admin.site.register(State)
admin.site.register(District)