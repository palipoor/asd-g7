import simplejson
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from apps.location.models import State, City, District


def get_cities(request, state_id=0):
	state = State.objects.get(pk=state_id)
	cities = City.objects.filter(state=state)
	cities_dict = {}
	for city in cities:
		cities_dict[city.id] = city.city

	cities_dict[0] = 'همهٔ شهرها'
	h = HttpResponse(simplejson.dumps(cities_dict),content_type='application/json')
	return h


def get_districts(request, city_id=0):
	print('1')
	city = City.objects.get(pk=city_id)
	districts = District.objects.filter(city=city)
	dist_dict = {}
	for dist in districts:
		dist_dict[dist.id] = dist.district

	dist_dict[0] = 'همهٔ محله‌ها'

	h = HttpResponse(simplejson.dumps(dist_dict),content_type='application/json')
	return h