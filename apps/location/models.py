from django.db import models


# Create your models here.
class State(models.Model):
	state = models.CharField(max_length=50, verbose_name="استان")

	class Meta:
		verbose_name = 'استان'
		verbose_name_plural = 'استان‌ها'

	def __str__(self):
		return self.state


class City(models.Model):
	city = models.CharField(max_length=50, verbose_name="شهر")
	state = models.ForeignKey(State, verbose_name="استان", on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'شهر'
		verbose_name_plural = 'شهرها'

	def __str__(self):
		return self.city


class District(models.Model):
	district = models.CharField(max_length=50, verbose_name="محله")
	city = models.ForeignKey(City, verbose_name="شهر", on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'محله'
		verbose_name_plural = 'محله‌ها'

	def __str__(self):
		return self.district


class Location(models.Model):
	customer = models.ForeignKey("registeration.Customer", verbose_name='مشتری', on_delete=models.CASCADE)
	state = models.ForeignKey(State, verbose_name='استان', on_delete=models.CASCADE)
	city = models.ForeignKey(City, verbose_name='شهر', on_delete=models.CASCADE)
	district = models.ForeignKey(District, verbose_name='محله', on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'مکان'
		verbose_name_plural = 'اماکن'

	def __str__(self):
		return str(self.state) +'-'+ str(self.city) + '-' + str(self.district)