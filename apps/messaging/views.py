from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView

from apps.notification.models import Notification
from apps.product.models import Product


class MessageCreateView(LoginRequiredMixin,CreateView):
	model = Notification
	template_name = "messaging/create_message.html"
	fields = [
		"title",
		"text",
	]

	def get_success_url(self):
		# TODO use reverse
		# return reverse_lazy("product:product-detail",self.kwargs["pk"])
		return "/product-details/" + str(self.kwargs["pk"])

	def form_valid(self, form):
		source_user = self.request.user
		form.instance.sender_user = source_user
		dest_user = Product.objects.get(id=self.kwargs["pk"]).creator
		form.instance.user = dest_user
		form.instance.seen = False
		return super().form_valid(form)