from django.conf.urls import url
from django.urls import path

from apps.messaging.views import MessageCreateView

urlpatterns = [
	path('create-message/<int:pk>', MessageCreateView.as_view(), name='create-message'),

]
