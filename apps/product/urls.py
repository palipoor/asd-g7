from apps.product.views import *
from django.urls import path

from apps.shopping.views import BuyProductView

urlpatterns = [
	path('product-details/<int:pk>', ProductDetailView.as_view(), name='product-details'),
	path('product-details/<int:pk>', ProductDetailView.as_view(), name='product-details'),
	path('create-product', ProductCreateView.as_view(), name='create-product'),
	path('comparison/<int:pk1>,<int:pk2>', ProductCompareView.as_view(), name='comparison'),
	path('helpers/son_categories/<int:category_id>', get_category_childs_one_level,
		 name='get_category_childs_one_level')
]
