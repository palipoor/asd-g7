import simplejson
from django.http import HttpResponse
from django.views import View
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from dateutil import relativedelta
import datetime
from apps.location.models import *
from apps.notification.models import Notification
from apps.product.models import Product, Comment, Category
from apps.product.forms import ComparisonForm, ProductCreationForm
from apps.registeration.models import Customer
from apps.auction.models import Auction
from apps.shopping.models import Transaction
import jdatetime

class ProductCompareView(FormView):
	form_class = ComparisonForm
	template_name = "product/comparison.html"

	def get_context_data(self, **kwargs):
		data = super().get_context_data(**kwargs)
		self.pk1 = self.kwargs["pk1"]
		self.pk2 = self.kwargs["pk2"]
		try:
			product1 = Product.objects.get(id=self.pk1)
		except Exception:
			product1 = None
		try:
			product2 = Product.objects.get(id=self.pk2)
		except Exception:
			product2 = None
		data["product1"] = product1
		data["product2"] = product2
		return data

	def form_valid(self, form):
		product1 = form.cleaned_data.get("product1")
		product2 = form.cleaned_data.get("product2")
		self.pk1 = product1.id
		self.pk2 = product2.id
		return super().form_valid(form)

	def get_success_url(self):
		# TODO use reverse
		# return reverse_lazy("product:comparison",self.pk1,self.pk2)
		return str(self.pk1) + "," + str(self.pk2)

	def get_initial(self):
		"""
		Returns the initial data to use for forms on this view.
		"""
		initial = super().get_initial()
		initial['product1'] = self.kwargs["pk1"]
		initial['product2'] = self.kwargs["pk2"]

		return initial


class ProductDetailView(CreateView):
	model = Comment
	template_name = "product/product_details.html"

	def get_success_url(self):
		# TODO use reverse
		# return reverse_lazy("product:product-detail",self.kwargs["pk"])
		return str(self.kwargs["pk"])

	fields = [
		"title",
		"text",
	]

	def get_context_data(self, **kwargs):
		data = super().get_context_data(**kwargs)
		try:
			data["object"] = Product.objects.get(id=self.kwargs["pk"])
			self.product = data["object"]

		except Exception as e:
			print("faiielll")
		comments = Comment.objects.filter(product=self.product).order_by('date')
		data["comments"] = comments
		if self.product.is_auction:
			data["date"] =  self.product.auction.finish_date.togregorian()
			data["send_date"] =  self.product.auction.send_date.togregorian()
			data["pay_date"] =  self.product.auction.pay_date.togregorian()
		related_category = self.product.category
		if related_category.parent:
			related_category = related_category.parent
		data["relateds"] = Product.objects.filter(category__in=related_category.get_category_and_its_children())[:5]
		return data

	def form_valid(self, form):
		user = self.request.user
		form.instance.user = Customer.objects.get(id = user.id)
		form.instance.product = Product.objects.get(id=self.kwargs["pk"])
		return super().form_valid(form)


class ProductCreateView(LoginRequiredMixin, CreateView):
	form_class = ProductCreationForm
	template_name = "product/create_product.html"

	def get_success_url(self):
		if self.is_auction:
			return reverse_lazy("auction:create-auction", kwargs={'pk': self.object.auction.id })
		else:
			return reverse_lazy("landing:landing")
	
	def post(self, request, *args, **kwargs):
		self.object = None
		form = self.get_form()
		creator = Customer.objects.get(username = self.request.user.username)
		form.instance.creator = creator
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		user = self.request.user
		customer = Customer.objects.get(username=user.username)
		form.instance.creator = customer
		self.is_auction = form.instance.is_auction
		finish_date = datetime.datetime.now() + relativedelta.relativedelta(days=28)
		pay_date = datetime.datetime.now() + relativedelta.relativedelta(days=30)
		send_date = datetime.datetime.now() + relativedelta.relativedelta(days=32)
		start_transaction = Transaction(amount = form.instance.price/50, source_user = form.instance.creator, type = 'ودیعه')
		start_transaction.save()
		start_transaction = Transaction.objects.get(id = start_transaction.id)
		auction = Auction(finish_date = finish_date, pay_date = pay_date, send_date = send_date, price = form.instance.price, start_transaction=start_transaction)
		auction.save()
		auction = Auction.objects.get(pk = auction.pk)
		form.instance.auction = auction
		return super(ProductCreateView, self).form_valid(form)

	def form_invalid(self, form):
		return super().form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super(ProductCreateView, self).get_context_data(**kwargs)
		context['first_category'] = {c['id']: c['name'] for c in Category.get_categories_json(-1)[0]['succs']}
		context['second_category'] = {}
		context['third_category'] = {}

		context['first_category'][0] = "سایر"
		context['second_category'][0] = "سایر"
		context['third_category'][0] = "سایر"
		context['locations'] = Location.objects.filter(customer_id=self.request.user.id)
		return context


def get_category_childs_one_level(request, category_id=1):
	print('was here ! :D')
	category = Category.objects.get(id=category_id)
	family = category.get_level_one_children()
	cat_dict = {}
	for cat in family:
		cat_dict[cat.id] = cat.name

	cat_dict[0] = 'سایر'
	h = HttpResponse(simplejson.dumps(cat_dict), content_type='application/json')
	return h
