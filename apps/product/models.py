from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MinLengthValidator
from taggit.managers import TaggableManager
from django.urls import reverse_lazy
from django.apps import apps
from django_jalali.db import models as jmodels
from apps.location.models import State, City, District
from apps.registeration.models import Customer
from backend_settings import settings
from utilities.notification_tools import notify
from utilities.validators import validate_existing_category, validate_customer_has_credit
# Create your models here.
from utilities.storage import OverwriteStorage


class Category(models.Model):
	parent = models.ForeignKey('self', verbose_name="دسته بندی پدر", on_delete=models.CASCADE, null=True)
	name = models.CharField("نام", max_length=50, unique=True)
	description = models.TextField("توضیحات", max_length=50, default="توضیحی ندارد.")
	
	class Meta:
		verbose_name = 'دسته'
		verbose_name_plural = 'دسته‌ها'

	def get_url(self, ):
		return reverse_lazy("landing:search", kwargs={"categoryid": self.id})

	def get_category_and_its_children(self):
		result = [self]
		children = Category.objects.filter(parent=self)
		for child in children:
			result += child.get_category_and_its_children()
		return result

	def get_level_one_children(self):
		children = Category.objects.filter(parent=self)
		return children

	@staticmethod
	def get_categories_json(dest):
		root = Category.objects.get(parent=None)
		successors = Category.objects.filter(parent=root)
		# result = [Category._get_category_json(succ, dest) for succ in successors]
		return [Category._get_category_json(root, dest)]

	@staticmethod
	def _get_category_json(node, dest):
		successors = Category.objects.filter(parent=node)
		json = {
			"id": node.id,
			"name": node.name,
			"desc": node.description,
			"is_active": node.id == dest,
			"url": node.get_url(),
			"succs": [],
		}
		for succ in successors:
			temp = Category._get_category_json(succ, dest)
			if temp["is_active"]:
				json["is_active"] = True
			json["succs"].append(temp)

		return json

	def __str__(self):
		return self.name


product_state_choices = (('کاملا نو', 'کاملا نو'),
						 ('در حد نو', 'در حد نو'),
						 ('استفاده شده', 'استفاده شده'),
						 ('کهنه', 'کهنه')
						 )


class Product(models.Model):
	name = models.CharField("محصول", max_length=50)
	creator = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
	price = models.IntegerField("قیمت", validators=[MinValueValidator(0, message="مقدار داده شده باید مثبت باشد.")])
	quantity = models.IntegerField("تعداد کل",
								   validators=[
									   MinValueValidator(0, message="مقدار داده شده باید عدد صحبح مثبت باشد.")])
	remaining = models.IntegerField("تعداد باقی‌مانده", blank=True, null=True)
	description = models.TextField("توضیحات",
								   validators=[MinLengthValidator(50, "توضیحات شما باید حداقل ۵۰ کاراکتر باشد")])
	district = models.ForeignKey(District, verbose_name="محله", null=True, on_delete=models.SET_NULL, blank=True)
	city = models.ForeignKey(City, verbose_name="شهر", null=True, on_delete=models.SET_NULL, blank=True)
	state = models.ForeignKey(State, verbose_name="استان", null=True, on_delete=models.SET_NULL, blank=True)
	category = models.ForeignKey(Category, verbose_name="دسته", on_delete=models.SET_NULL, null=True)
	image = models.ImageField(verbose_name="تصویر", null=True, storage=OverwriteStorage())
	product_state = models.CharField(verbose_name='وضعیت', choices=product_state_choices, max_length=20, null=True)
	material = models.CharField(verbose_name='جنس', max_length=20, null=True)
	is_trending = models.BooleanField(default=False,verbose_name='آیا ترند است')
	is_auction = models.BooleanField(default=False,verbose_name='آیا مزایده است')
	auction = models.OneToOneField("auction.Auction", verbose_name="حراج", null=True, on_delete=models.SET_NULL, blank=True)
	tags = TaggableManager(verbose_name='برچسب‌ها', help_text="لیستی از تگ ها که با ',' جدا شده اند")
	suggested_new_category = models.CharField(verbose_name="نام دستهٔ پیشنهادی جدید", max_length=50,
							null = True,blank = True, help_text="در صورتی که به نظرتان دستهٔ مورد نظر شما در دسته‌های بالا موجود نبود نام دستهٔ مورد نظر خود را وارد کنید.", validators = [validate_existing_category])

	class Meta:
		verbose_name = 'کالا'
		verbose_name_plural = 'کالاها'

	def clean(self):
		if self.is_auction:
			validate_customer_has_credit(self.creator,self.price/10)
		return super().clean()

	def __str__(self):
		return self.name + " به شناسهٔ " + str(self.id)

	@property
	def location(self):
		result = ""
		if self.district:
			result += str(self.district) + ","
		if self.city:
			result += str(self.city) + ","
		if self.state:
			result += str(self.state) + ","
		return result

	@property
	def url(self):
		return reverse_lazy('product:product-details', kwargs={"pk": self.id})

	@property
	def remained(self):
		return self.remaining > 0

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		
		if self.pk is None:
			self.remaining = self.quantity
			print(self.remaining)
			result = super().save(force_insert=force_insert, force_update=force_update, using=using,
								  update_fields=update_fields)

			print(self.remaining)

			qs = apps.get_model("wishlist", "Wishlist").objects.all()

			qs = qs.exclude(min_price__gte=self.price)
			qs = qs.exclude(min_price__lte=self.price)

			if self.state:
				qs = qs.filter(state=self.state) | qs.filter(state=None)

			if self.city:
				qs = qs.filter(city=self.city) | qs.filter(city=None)
			if self.district:
				qs = qs.filter(district=self.district) | qs.filter(district=None)
			if self.tags:
				qs = qs.filter(tags__name__in=self.tags.names()) | qs.filter(tags=None)

			if self.category:
				qs = qs.filter(category=self.category) | qs.filter(category=None)
			temp = []
			target = self.name.split() + self.description.split()
			for wishlist in qs:
				if wishlist.text:
					counter = 0
					words = wishlist.text.split()
					for word in words:
						if word in target:
							counter += 1
					if counter / len(words) > 0.6:
						temp.append(wishlist)
				else:
					temp.append(wishlist)
			qs = temp
			for wishlist in qs:
				message = "اضافه شدن کالای درخواستی به سامانه\n" + "کالای " + self.name + "به شماره‌ٔ شناسهٔ " + str(
					self.id) + " مربوط به کالاهای مورد نظر شما است."
				notify("اضافه شدن کالای جدید به سامانه", message, wishlist.user.username)
		else:
			result =  super().save(force_insert=force_insert, force_update=force_update, using=using,
							update_fields=update_fields)

		if not self.suggested_new_category == None:
				RequestedCategory(name = self.suggested_new_category, product = Product.objects.get(pk = self.pk), parent_category = self.category).save()

		return result


class Comment(models.Model):
	user = models.ForeignKey(Customer, verbose_name="کاربر", on_delete=models.CASCADE)
	product = models.ForeignKey(Product, verbose_name="کالا", on_delete=models.CASCADE)
	title = models.CharField("عنوان", max_length=50)
	text = models.TextField("متن", validators=[MinLengthValidator(50, "توضیحات شما باید حداقل ۵۰ کاراکتر باشد")])
	date = jmodels.jDateTimeField(auto_now_add=True)

class RequestedCategory(models.Model):
	name = models.CharField("عنوان", max_length=50)
	product = models.ForeignKey(Product, verbose_name="کالا", on_delete=models.CASCADE)
	parent_category = models.ForeignKey(Category, verbose_name="دستهٔ پدر", on_delete=models.SET_NULL, null=True)
	seen = models.BooleanField(verbose_name="بررسی شده", default=False)
	implemented = models.BooleanField(verbose_name="ساخته شود", default=False)

	class Meta:
		verbose_name = 'پیشنهاد دسته'
		verbose_name_plural = 'پیشینهاد‌های دسته'

	def __str__(self):
		return self.name

	def clean(self):
		if self.implemented:
			validate_existing_category(self.name)
		return super().clean()

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		if self.pk:
			original = RequestedCategory.objects.get(pk = self.pk)
			if self.implemented and not original.implemented:
				self.seen = True
				new_cat = Category(parent = self.parent_category, name = self.name)
				new_cat.save()
				new_cat = Category.objects.get(pk = new_cat.pk)
				self.product.category = new_cat
				self.product.suggested_new_category = None
				self.product.save()
		return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)