from django.contrib import admin
from apps.product.models import Product,Category,RequestedCategory
from django.contrib.auth.models import Group
from taggit.models import Tag

admin.site.register(Product)
admin.site.register(Category)
admin.site.register(RequestedCategory)
admin.site.unregister(Tag)
admin.site.unregister(Group)


