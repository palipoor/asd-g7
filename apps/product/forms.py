from django import forms
from django.apps import apps
from django.core.exceptions import ValidationError

from apps.location.models import City, State, District
from apps.product.models import Product, Category


class ComparisonForm(forms.Form):
	product1 = forms.ModelChoiceField(queryset=apps.get_model("product", "Product").objects.all(), label="کالای اول")
	product2 = forms.ModelChoiceField(queryset=apps.get_model("product", "Product").objects.all(), label="کالای دوم")


class ProductCreationForm(forms.ModelForm):
	location = forms.CharField(label='مکان', required=True)
	first_category = forms.CharField(label='دسته‌بندی سطح اول', required=True)
	second_category = forms.CharField(label='دسته‌بندی سطح دوم', required=False)
	third_category = forms.CharField(label='دسته‌بندی سطح سوم', required=False)
	class Meta:
		model = Product
		fields = [
			"name",
			"price",
			"quantity",
			"description",
			"product_state",
			"material",
			"suggested_new_category",
			"tags",
			"is_auction",
			"image"
		]

	def clean(self):
		cleaned_data = super().clean()
		location_parts = cleaned_data['location'].split('-')
		state = location_parts[0]
		city = location_parts[1]
		district = location_parts[2]
		self.instance.city = City.objects.get(city=city)
		self.instance.state = State.objects.get(state=state)
		self.instance.district = District.objects.get(district=district)

		print(cleaned_data)
		if cleaned_data.get('third_category'):
			self.instance.category = Category.objects.get(id=int(cleaned_data.get('third_category')))
		elif cleaned_data.get('second_category'):
			self.instance.category = Category.objects.get(id=int(cleaned_data.get('second_category')))
		else:
			raise ValidationError(message = 'شما باید حداقل دو سطح دسته‌بندی انتخاب کنید.')