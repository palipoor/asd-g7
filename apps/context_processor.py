from apps.notification.models import Notification

translate = {
	'1': '۱',
	'2': '۲',
	'3': '۳',
	'4': '۴',
	'5': '۵',
	'6': '۶',
	'7': '۷',
	'8': '۸',
	'9': '۹',
	'0': '۰'
}


def notifs(request):
	print('here! ')
	if request.user:
		if request.user.is_authenticated:
			print('also here! ')
			notifs = Notification.objects.filter(user__username=request.user.username, seen=False)
			print(notifs)
			count = str(len(notifs))
			persian_count = ''
			for c in count:
				persian_count += translate.get(c)
			return {'notif_count': persian_count}

	return {}
