from django.shortcuts import render
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from apps.auction.models import Auction
from django_jalali.forms.widgets import jDateTimeInput
from django import forms
from django.urls import reverse_lazy
from apps.auction.models import Bid
from apps.product.models import Product
from apps.registeration.models import Customer
from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import loader
from apps.shopping.models import Transaction


# Create your views here.

def recieved(request, id):
	auction = Auction.objects.get(id=id)
	auction.recieved()
	auction.save()
	return message(request, "با موفقیت دریافت شد.")


def message(request, message):
	template = loader.get_template('auction/message.html')
	context = {}
	context["message"] = message
	return HttpResponse(template.render(context, request))


def pay(request, id):
	user = request.user
	customer = Customer.objects.get(username=user.username)
	auction = Auction.objects.get(id=id)
	print(auction.price)
	print(customer.credit)
	if 0.9 * auction.price > customer.credit:
		# return redirect(reverse_lazy("auction:message","پرداخت با موفقیت انجام شد."))		
		return message(request, "موجودی حساب شماکافی نیست.")
	else:
		# return redirect(reverse_lazy("auction:message","موجودی حساب شماکافی نیست."))
		auction.pay()
		auction.save()
		return message(request, "پرداخت با موفقیت انجام شد.")


def decline_pay(request, id):
	auction = Auction.objects.get(id=id)
	auction.buyer_decline()
	auction.save()
	return redirect(reverse_lazy("dashboard:my_products"))


def send(request, id):
	auction = Auction.objects.get(id=id)
	auction.send()
	auction.save()
	return message(request, "با موفقیت اعلام ارسال کردید.")


def decline_send(request, id):
	auction = Auction.objects.get(id=id)
	auction.seller_decline()
	auction.save()
	return message(request, "با موفقیت انصراف دادید.")


def dropout(request, id):
	auction = Auction.objects.get(id=id)
	auction.dropout()
	auction.save()
	return redirect(reverse_lazy("dashboard:bought_products"))


class CreateBidView(LoginRequiredMixin, CreateView):
	model = Bid
	template_name = "auction/create_bid.html"
	fields = [
		"value",
	]

	def post(self, request, *args, **kwargs):
		self.object = None
		form = self.get_form()
		creator = Customer.objects.get(username=self.request.user.username)

		form.instance.creator = creator
		form.instance.auction = Auction.objects.get(pk=self.kwargs["auction"])
		product = Product.objects.get(auction=form.instance.auction)
		self.success_url = product.url
		form.instance.deposit = Transaction(amount=int(request.POST["value"]) / 10, source_user=creator, type='ودیعه')
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		# form.instance.depos
		return super().form_valid(form)


class AuctionUpdateView(LoginRequiredMixin, UpdateView):
	model = Auction
	template_name = "auction/create_auction.html"
	fields = [
		"finish_date",
		"pay_date",
		"send_date",
	]

	def get_success_url(self):
		id = self.kwargs.get('pk')
		product = Product.objects.get(auction__id=id)
		return product.url

	def form_valid(self, form):
		return super().form_valid(form)
