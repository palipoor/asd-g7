from django.apps import AppConfig
from backend_settings import settings


class AuctionConfig(AppConfig):
	name = 'apps.auction'

	def ready(self):
		print("in here")
		from backend_settings.scheduler import project_scheduler
		if settings.SCHEDULER_AUTOSTART:
			print("in here")
			project_scheduler.start()
