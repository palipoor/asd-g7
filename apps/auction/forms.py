from django.forms import ModelForm, DateTimeField
from django_jalali.forms import jDateTimeInput, jDateTimeField

from apps.auction.models import Auction


class AuctionEditForm(ModelForm):
	class Meta:
		model = Auction
		fields = ['date', ]

	date = DateTimeField(widget=jDateTimeInput(attrs={
		'class': 'form-control datetimepicker-input',
		'id': 'auctiondatetime'
	}), required=True)
