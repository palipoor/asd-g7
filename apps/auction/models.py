from django.db import models
from apps.shopping.models import Transaction
from apps.registeration.models import Customer
from django_jalali.db import models as jmodels
from django.core.validators import MinValueValidator
from datetime import datetime
from django.core.exceptions import ValidationError
from enum import Enum
from django.apps import apps
from backend_settings.scheduler import project_scheduler
# Create your models here.
import pytz


class AuctionStatusChoices(Enum):  # A subclass of Enum
	active = "در حال انجام"
	to_pay = "در انتظار پرداخت"
	success = "موفق"
	to_recieve = "در انتظار دریافت"
	to_send = "در انتظار فرستادن"
	customer_fail = "ناموفق: عدم پرداخت خریدار"
	seller_fail = "ناموفق: عدم تحویل فروشنده"


class Auction(models.Model):
	finish_date = jmodels.jDateTimeField(verbose_name="زمان پایان مزایده",
										 help_text="به صورت پیش‌فرض مزایده‌ها چهار هفته‌ای هستند.")
	pay_date = jmodels.jDateTimeField(verbose_name="دیرترین زمان پرداخت",
									  help_text="به صورت پیش‌فرض دیرترین زمان پرداخت دو روز پس از مزایده است.")
	send_date = jmodels.jDateTimeField(verbose_name="دیرترین زمان فرستادن",
									   help_text="به صورت پیش‌فرض دیرترین زمان ارسال چهار روز پس از مزایده است.")
	price = models.IntegerField("کف قیمت", validators=[MinValueValidator(0, message="مقدار داده شده باید مثبت باشد.")])
	final_transaction = models.ForeignKey(Transaction, verbose_name="پرداخت نهایی", on_delete=models.CASCADE, null=True,
										  related_name="پرداخت")
	recieved_transaction = models.ForeignKey(Transaction, verbose_name="دریافت نهایی", on_delete=models.CASCADE,
											 null=True, related_name="دریافت")
	start_transaction = models.ForeignKey(Transaction, verbose_name="ودیعهٔ برگزار کنندهٔ مزایده",
										  on_delete=models.CASCADE, null=True, related_name="ودیعه")
	status = models.CharField(
		max_length=50,
		choices=[(tag.value, tag.value) for tag in AuctionStatusChoices],
		default=AuctionStatusChoices.active.value
	)

	# def dropout(self):
	#     print("in dropout")
	#     if self.status == AuctionStatusChoices.pening.value:
	#         print("dropping")
	#         highest_bid = Bid.objects.filter(auction = self).latest('value')
	#         product = apps.get_model("product","Product").objects.get(auction = self)
	#         print(product.creator,highest_bid.creator)
	#         transaction = Transaction(amount = self.price/50, source_user = product.creator, dest_user = highest_bid.creator)
	#         transaction.save()
	#         self.recieved_transaction = Transaction.objects.get(id = transaction.id)
	#         self.recieved_transaction.apply()
	#         print(self.recieved_transaction)
	#         temp = self.final_transaction.get_reverse_transaction()
	#         temp.save()
	#         temp = Transaction.objects.get(id = temp.id)
	#         temp.apply()
	#         print(temp)
	#         self.status = AuctionStatusChoices.success.value

	def recieved(self):
		if self.status == AuctionStatusChoices.to_recieve.value:
			product = apps.get_model("product", "Product").objects.get(auction=self)
			transaction = self.start_transaction.get_reverse_transaction()
			transaction.save()
			transaction = Transaction.objects.get(id=transaction.id)
			transaction.apply()
			transaction = Transaction(amount=self.price, dest_user=product.creator, type="خرید")
			transaction.save()
			self.recieved_transaction = Transaction.objects.get(id=transaction.id)
			self.recieved_transaction.apply()
			self.status = AuctionStatusChoices.success.value

	def pay(self):
		if self.status == AuctionStatusChoices.to_pay.value:
			print("in payment")
			product = apps.get_model("product", "Product").objects.get(auction=self)
			highest_bid = Bid.objects.filter(auction=self).latest('value')
			print("in payment")
			transaction = highest_bid.deposit.get_reverse_transaction()
			transaction.save()
			transaction = Transaction.objects.get(id=transaction.id)
			transaction.apply()
			print("in payment")
			transaction = Transaction(amount=self.price, source_user=highest_bid.creator, type='خرید')
			transaction.save()
			print("in payment")
			self.final_transaction = Transaction.objects.get(id=transaction.id)
			self.final_transaction.apply()
			print("in payment")
			self.status = AuctionStatusChoices.to_send.value

	def send(self):
		if self.status == AuctionStatusChoices.to_send.value:
			# product = apps.get_model("product","Product").objects.get(auction = self)
			# highest_bid = Bid.objects.filter(auction = self).latest('value')
			# transaction = self.start_transaction.get_reverse_transaction()
			# transaction.save()
			# transaction = Transaction.objects.get(id = transaction.id)
			# transaction.apply()
			# transaction = Transaction(amount = self.price, dest_user = product.creator)
			# transaction.save()
			# self.recieved_transaction = Transaction.objects.get(id = transaction.id)
			# self.recieved_transaction.apply()
			self.status = AuctionStatusChoices.to_recieve.value

	def seller_decline(self):
		transaction = self.start_transaction.get_reverse_transaction()
		transaction.save()
		transaction = Transaction.objects.get(id=transaction.id)
		transaction.apply()
		product = apps.get_model("product", "Product").objects.get(auction=self)
		highest_bid = Bid.objects.filter(auction=self).latest('value')
		self.status = AuctionStatusChoices.seller_fail.value
		transaction = Transaction(source_user=product.creator,
								  dest_user=highest_bid.creator,
								  amount=product.price,
								  type='جریمهٔ انصراف',
								  )
		transaction.save()
		self.recieved_transaction = Transaction.objects.get(id=transaction.id)
		self.recieved_transaction.apply()

	def check_send(self):
		if self.status == AuctionStatusChoices.to_send.value:
			self.seller_decline()

	def buyer_decline(self):
		product = apps.get_model("product", "Product").objects.get(auction=self)
		highest_bid = Bid.objects.filter(auction=self).latest('value')
		transaction = highest_bid.deposit.get_reverse_transaction()
		transaction.save()
		transaction = Transaction(id=transaction.id)
		transaction.apply()
		self.status = AuctionStatusChoices.customer_fail.value
		transaction = Transaction(source_user=highest_bid.creator,
								  dest_user=product.creator,
								  amount=product.price,
								  type='جریمهٔ انصراف',
								  )
		transaction.save()
		self.recieved_transaction = Transaction.objects.get(id=transaction.id)
		self.recieved_transaction.apply()
		self.recieved_transaction.save()

	def check_pay(self):
		if self.status == AuctionStatusChoices.to_pay.value:
			self.buyer_decline()

	def isFinished(self):
		print("che")
		print(datetime.now(pytz.timezone('Asia/Tehran')))
		print(self.finish_date.togregorian())
		return not self.status == AuctionStatusChoices.active.value or datetime.now(
			pytz.timezone('Asia/Tehran')) >= self.finish_date.togregorian()

	def finish(self):
		print("going to finish")
		bids = Bid.objects.filter(auction=self)
		product = apps.get_model("product", "Product").objects.get(auction=self)
		if len(bids) > 0:
			print("having bids")
			highest_bid = bids.latest('value')
			print(bids)
			bids = bids.exclude(id=highest_bid.id)
			for bid in bids:
				temp = bid.deposit.get_reverse_transaction()
				temp.save()
				temp = Transaction.objects.get(pk=temp.pk)
				temp.apply()
			print("changing status")
			self.status = AuctionStatusChoices.to_pay.value
			print(self.status)
			shop_record = apps.get_model("shopping", "ShopRecord")(buyer=highest_bid.creator, product=product,
																   quantity=product.quantity, price=highest_bid.value)
			shop_record.save()
			# seller = apps.get_model("product","Product").objects.get(auction = self).creator
			# highest_bid.value
			# Transaction(amount = highest_bid.value, source_user = highest_bid.creator)
			# self.final_transaction = Transaction(amount = highest_bid.value, source_user = highest_bid.creator)
			# try:
			#     self.final_transaction.full_clean()
			#     self.final_transaction.save()
			#     self.final_transaction = Transaction.objects.get(pk = self.final_transaction.pk)
			#     self.final_transaction.apply()
			#     self.status = AuctionStatusChoices.pening.value
			#     shop_record = apps.get_model("shopping","ShopRecord")(buyer = highest_bid.creator, product = product, quantity = product.quantity, price = highest_bid.value)
			#     shop_record.save()
			#     print("AAA")
			# except ValidationError as err:
			#     self.final_transaction = Transaction(amount = highest_bid.value/50, source_user = highest_bid.creator)
			#     self.final_transaction.full_clean()
			#     self.final_transaction.save()
			#     self.final_transaction = Transaction.objects.get(pk = self.final_transaction.pk)
			#     self.final_transaction.apply()
			#     transaction = Transaction(amount = highest_bid.value/50, dest_user = product.creator)
			#     transaction.save()
			#     self.recieved_transaction = Transaction.objects.get(id = transaction.id)
			#     self.recieved_transaction.apply()
			#     self.status = AuctionStatusChoices.customer_fail.value
			#     print("BBB")
		else:
			self.status = AuctionStatusChoices.success.value

	def create_finish_trigger(self):
		project_scheduler.add_auction_finish(self)

	def create_pay_trigger(self):
		project_scheduler.add_auction_pay(self)

	def create_send_trigger(self):
		project_scheduler.add_auction_send(self)

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		is_new = self.id == None
		temp = Auction.objects.filter(id=self.id)
		print("temp is ", temp)
		if (len(temp) > 0 and temp[0].finish_date != self.finish_date):
			print("tried finish")
			self.create_finish_trigger()

		if (len(temp) > 0 and temp[0].send_date != self.send_date):
			print("tried send")
			self.create_send_trigger()

		if (len(temp) > 0 and temp[0].pay_date != self.pay_date):
			print("tried pay")
			self.create_pay_trigger()

		if self.id and self.status == AuctionStatusChoices.active.value and self.isFinished():
			self.finish()
		if self.final_transaction:
			self.final_transaction.save()
		temp = super().save(force_insert=force_insert, force_update=force_update, using=using,
							update_fields=update_fields)
		if is_new:
			Auction.objects.get(id=self.id).create_finish_trigger()
		return temp


class Bid(models.Model):
	creator = models.ForeignKey(Customer, verbose_name="پیشنهاد دهنده", on_delete=models.CASCADE)
	date = jmodels.jDateTimeField(auto_now_add=True, verbose_name="تاریخ")
	auction = models.ForeignKey(Auction, verbose_name="حراج", on_delete=models.CASCADE)
	value = models.IntegerField(verbose_name="قیمت",
								validators=[MinValueValidator(0, message="مقدار داده شده باید مثبت باشد.")])
	deposit = models.ForeignKey(Transaction, verbose_name="ودیعه", on_delete=models.CASCADE)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		try:
			print(self.deposit)
		except Exception as exp:
			if not self.value == None:
				self.deposit = Transaction(amount=self.value / 10, source_user=self.creator, type='ودیعه')

	def clean(self):
		if self.value <= self.auction.price:
			raise ValidationError("مبلغ پیشنهادی باید از کف مشارکت در مزایده بیشتر باشد.")
		if self.auction.isFinished():
			self.auction.save()
			raise ValidationError("حراج به پایان رسیده است.")
		print("christ")
		try:
			print(self.deposit)
		except Exception as exp:
			self.deposit = Transaction(amount=self.value / 10, source_user=self.creator, dest_user=None)
		print("christ")
		self.deposit.full_clean()
		print("christ")
		return super().clean()

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		if not self.pk:
			self.deposit.apply()
		if self.auction.price < self.value:
			self.auction.price = self.value
		self.auction.save()
		self.auction = Auction.objects.get(pk=self.auction.pk)
		self.deposit.save()
		self.deposit = Transaction.objects.get(id=self.deposit.id)
		print(self.pk)
		print(self.deposit)
		return super().save(force_insert=force_insert, force_update=force_update, using=using,
							update_fields=update_fields)
