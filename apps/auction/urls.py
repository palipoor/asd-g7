from apps.auction.views import *
from django.urls import path

urlpatterns = [
	path('create-bid/<int:auction>', CreateBidView.as_view(), name='create-bid'),
	path('create-auction/<int:pk>', AuctionUpdateView.as_view(), name='create-auction'),
	path('recieved/<int:id>', recieved, name='recieved'),
	path('pay/<int:id>', pay, name='pay'),
	path('decline-pay/<int:id>', decline_pay, name='decline-pay'),
	path('send/<int:id>', send, name='send'),
	path('decline-send/<int:id>', decline_send, name='decline-send'),
	path('dropout/<int:id>', dropout, name='dropout'),
]



