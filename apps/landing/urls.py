from django.urls import path

from apps.landing.views import LandingView, SearchResultView

urlpatterns = [
	path('', LandingView.as_view(), name='landing'),
	path('search/<int:categoryid>/', SearchResultView.as_view(), name='search'),
]
