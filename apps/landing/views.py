import random


from django.shortcuts import redirect
from django.views.generic import ListView
from django.contrib.postgres.search import SearchVector
from taggit.models import Tag

from apps.advertisement.models import Advertisement
from apps.product.models import Product, City, Category
from apps.location.models import State, District


class LandingView(ListView):
	template_name = 'landing/landing.html'
	context_object_name = 'products'
	model = Product

	def get_queryset(self):
		trend_products = Product.objects.filter(is_trending=True)
		return []

	def get_context_data(self, **kwargs):
		# print("in get data")
		context = super().get_context_data(**kwargs)
		context['categories'] = Category.get_categories_json(-1)
		context['tags'] = Tag.objects.all()
		context['states'] = State.objects.all()
		context['cities'] = sorted(City.objects.all(), key=lambda x: x.city)
		context['districts'] = []
		for district in District.objects.all():
			context['districts'].append(district.district + '-' + district.city.city)

		context['districts'] = sorted(context['districts'])
		context['max_price'] = 1000000  # TODO query over all produucts and get max price
		try:
			adv1s = Advertisement.objects.filter(place="بالا", page="صفحه اصلی")
			r = random.randint(0, len(adv1s) - 1)
			adver1 = adv1s[r]
			print(adv1s, r, adver1)
		except:
			adver1 = None
			print("hi")
		try:
			adv2s = Advertisement.objects.filter(place="پایین", page="صفحه اصلی")
			r = random.randint(0, len(adv2s) - 1)
			adver2 = adv2s[r]
		except:
			adver2 = None
		context['adv1'] = adver1
		context['adv2'] = adver2
		try:
			most_recent = Product.objects.all()
		except:
			most_recent = []
		try:
			trending = Product.objects.filter(is_trending=True)
		except:
			trending = []
		try:
			new = Product.objects.filter(product_state="کاملا نو")
		except:
			new = []
		print(most_recent)
		context['object_list_recent'] = reversed(most_recent[max(len(most_recent) - 9, 0):len(most_recent)])
		context['object_list_trending'] = reversed(trending[max(len(trending) - 9, 0):len(trending)])
		context['object_list_new'] = reversed(new[max(len(new) - 9, 0):len(new)])
		# print(context['categories'])
		return context

	def get(self, request, *args, **kwargs):
		search_string = self.request.GET.get('q', None)
		if not search_string == None:
			return redirect('search/0/?q=' + search_string)
		else:
			return super().get(request, *args, **kwargs)


statuses_dict = {0: 'فرقی نمی‌کند', 1: 'کاملا نو', 2: 'در حد نو', 3: 'استفاده شده', 4: 'کهنه'}


class SearchResultView(ListView):
	template_name = 'landing/search.html'
	context_object_name = 'products'

	def dispatch(self, request, *args, **kwargs):
		self.category = kwargs.pop('categoryid')
		return super().dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['selected_category'] = self.category
		context['categories'] = Category.get_categories_json(self.category)
		context['tags'] = Tag.objects.all()
		context['states'] = {state.id: state.state for state in State.objects.all()}
		context['cities'] = {}
		context['districts'] = {}
		context['statuses'] = statuses_dict
		if self.request.GET:
			state_id = int(self.request.GET.get("state", 1))
			city_id = int(self.request.GET.get("city", 1))
			district_id = int(self.request.GET.get("district", 1))
			print('city id', city_id)
			if not (city_id == "0" or state_id == 0 or city_id == 1):
				context['cities'] = {city.id: city.city for city in City.objects.filter(state=state_id)}

			if not (district_id == "0" or city_id == "0" or state_id == 0):
				context['districts'] = {district.id: district.district for district in
										District.objects.filter(city=city_id)}
		context['states'][0] = "همهٔ استان‌ها"
		context['districts'][0] = "همهٔ محله‌ها"
		context['cities'][0] = "همهٔ شهرها"
		context['cities'] = dict(sorted(context['cities'].items()))

		context['max_price'] = 1000000
		return context

	def get_queryset(self):
		search_string = self.request.GET.get('q', '')
		qs = Product.objects.all()
		if search_string != '':
			print(search_string)
			qs = Product.objects.annotate(
				search=SearchVector('name', 'description')
			).filter(search=search_string)
		min_price = self.request.GET.get('minimumprice', 0)

		if min_price:
			qs = qs.filter(price__gte=min_price)
		max_price = self.request.GET.get('maximumprice', 0)  # TODO set to max price of all products
		if max_price:
			qs = qs.filter(price__lte=max_price)
		state = self.request.GET.get('state', '')
		if state:
			if state != 'همهٔ استان‌ها' and state != "0":
				qs = qs.filter(state__id=state)

		city = self.request.GET.get('city', '')
		if city:
			if city != 'همهٔ شهرها' and city != "0":
				qs = qs.filter(city__id=city)

		district = self.request.GET.get('district', '')
		if district:
			if district != "همهٔ محله‌ها" and district != "0":
				qs = qs.filter(district__id=district)

		tags = self.request.GET.getlist('tags', [])
		if tags:
			qs = qs.filter(tags__name__in=tags)
		category = self.category
		if category:
			category_record = Category(id=category)
			qs = qs.filter(category__in=category_record.get_category_and_its_children())

		status = self.request.GET.get('product_status', '')
		if status and status != '0':
			qs = qs.filter(product_state=statuses_dict.get(int(status)))

		material = self.request.GET.get('material', '')
		if material:
			qs = qs.filter(material__icontains=material)
		print('akharin qs   *****************')
		print(qs)
		return qs
