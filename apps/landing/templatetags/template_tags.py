import datetime

import pendulum

TEHRAN_TIMEZONE = pendulum.timezone("Asia/Tehran")

import jdatetime
from django import template

register = template.Library()


@register.filter
def get_list(dictionary, key):
	return dictionary.getlist(key)



@register.filter(expects_localtime=True, is_safe=False)
def jformat(value, arg=None):
	"""Formats a date or time according to the given format."""
	if value in (None, ''):
		return ''
	if arg is None:
		arg = "%c"
	try:
		if isinstance(value, datetime.datetime):
			value = jdatetime.datetime.fromgregorian(datetime=value, tzinfo=TEHRAN_TIMEZONE)
		elif isinstance(value, datetime.date):
			value = jdatetime.date.fromgregorian(date=value, tzinfo=TEHRAN_TIMEZONE)
		return value.strftime(arg)
	except AttributeError:
		return ''
