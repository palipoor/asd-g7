from django.apps import AppConfig


class ShoppingConfig(AppConfig):
    name = 'apps.shopping'
    verbose_name = 'خرید'