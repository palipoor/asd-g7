from django.conf.urls import url
from django.urls import path

from apps.shopping.views import *

urlpatterns = [
	path('buy-product/<seller>&<prod>', BuyProductView.as_view(), name='buy'),

]
