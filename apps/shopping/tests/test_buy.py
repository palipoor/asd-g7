from django.contrib.auth import logout
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.urls import reverse_lazy

from apps.product.models import Product
from apps.shopping.models import Transaction, ShopRecord
from apps.location.models import State, City, District
from apps.registeration.models import Customer
import time


def create_customer(username, password):
	customer = Customer(username=username, credit=100000, email_verified=True)
	customer.set_password(password)
	customer.save()
	customer = Customer.objects.get(pk=customer.pk)
	return customer


def create_product(owner, quantity=1, price=1):
	product_name = 'prod' + str(time.time())
	product = Product(name=product_name, description='blah blah', quantity=quantity, creator=owner, price=price,
					  city=City.objects.get(id=1),
					  state=State.objects.get(id=1), district=District.objects.get(id=1))
	product.save()
	return product


def create_transaction(source_user, dest_user, amount):
	transaction = Transaction(source_user=source_user, dest_user=dest_user, amount=amount)
	try:
		transaction.clean()
		transaction.save()
		return transaction
	except Exception as e:
		raise e


class TransactionTest(TestCase):
	def test_valid_charge_transaction_is_created(self):
		customer = create_customer('fake_username', 'fake_password')
		customer_credit = customer.credit
		transaction = create_transaction(source_user=None, dest_user=customer, amount=10)
		transaction.clean()
		transaction.save()
		transaction.apply()
		transaction = Transaction.objects.get(id = transaction.id)
		self.assertEquals(customer.credit, customer_credit + 10)
		self.assertEquals(transaction.type, 'شارژ')

	def test_valid_buy_transaction_is_created(self):
		customer_1 = create_customer('fake_username1', 'fake_password1')
		customer_1_credit = customer_1.credit
		customer_2 = create_customer('fake_username2', 'fake_password2')
		customer_2_credit = customer_2.credit
		transaction = create_transaction(source_user=customer_1, dest_user=customer_2, amount=10)
		transaction.apply()
		transaction = Transaction.objects.get(id = transaction.id)
		self.assertEquals(customer_1_credit - customer_1.credit, customer_2.credit - customer_2_credit)
		self.assertEquals(customer_1.credit - customer_1_credit, -10)
		self.assertEquals(transaction.type, 'خرید')

	def test_invalid_buy_transaction_is_rejected(self):
		customer_1 = create_customer('fake_username3', 'fake_password3')
		customer_1_credit = customer_1.credit
		customer_2 = create_customer('fake_username4', 'fake_password4')
		customer_2_credit = customer_2.credit
		with self.assertRaises(ValidationError):
			create_transaction(source_user=customer_1, dest_user=customer_2, amount=500000)

		self.assertEquals(customer_1.credit, customer_1_credit)
		self.assertEquals(customer_2.credit, customer_2_credit)


def buy_get_request_data(seller_id, buyer_id, product_id, quantity):
	return {
		'seller': seller_id,
		'buyer': buyer_id,
		'prod': product_id,
		'quantity': quantity
	}


class BuyTest(TestCase):

	@classmethod
	def setUpClass(cls):
		super().setUpClass()

		cls.url = reverse_lazy('shopping:buy')
		cls.customer = create_customer('fake_username_user_name', 'fake_password_6')
		cls.customer_2 = create_customer('fake_username_not_user_name', 'fake_password_7')

	def test_buy_view_is_implemented(self):
		try:
			from apps.shopping.views import BuyProductView
		except:
			self.fail()

	def test_unathorized_user(self):

		product = create_product(quantity=10, owner=self.customer_2)
		buy_data = buy_get_request_data(seller_id=self.customer_2.id, buyer_id=self.customer.id, product_id=product.id,
										quantity=1)
		self.url = reverse_lazy('shopping:buy', args={'prod': product.id, 'seller'
		: self.customer_2.id})
		res = self.client.get(self.url, data=buy_data, follow=True)
		self.assertIn('/login', res.redirect_chain[0][0])

		def test_not_enough_credit(self):
			self.client.login(username=self.customer.username, password='fake_password_6')
			expensive_product = create_product(price=1000000, quantity=1, owner=self.customer)
			buy_data = buy_get_request_data(seller_id=self.customer_2.id, buyer_id=self.customer.id,
											product_id=expensive_product.id,
											quantity=1)
			print(buy_data)
			self.url = reverse_lazy('shopping:buy', args={'prod': expensive_product.id, 'seller'
			: self.customer_2.id})
			res = self.client.get(self.url, data=buy_data)

			self.assertIn('متاسفانه', res.body)

		def test_more_than_quantity(self):
			self.client.login(username=self.customer.username, password='fake_password_6')

			unique_product = create_product(price=10, quantity=1, owner=self.customer)
			buy_data = buy_get_request_data(seller_id=self.customer_2.id, buyer_id=self.customer.id,
											product_id=unique_product.id,
											quantity=2)
			self.url = reverse_lazy('shopping:buy', args={'prod': unique_product.id, 'seller'
			: self.customer_2.id})
			res = self.client.get(self.url, data=buy_data)
			self.assertIn('متاسفانه', res.body)

		def test_buy_works_correctly(self):
			self.client.login(username=self.customer.username, password='fake_password_6')

			product = create_product(price=15, quantity=2, owner=self.customer)
			seller_credit = self.customer.credit
			buyer_credit = self.customer_2.credit
			buy_data = buy_get_request_data(seller_id=self.customer_2.id, buyer_id=self.customer.id,
											product_id=product.id,
											quantity=1)
			args = {'prod': product.id, 'seller'
			: self.customer_2.id}
			self.url = reverse_lazy('shopping:buy', args={'prod': product.id, 'seller'
			: self.customer_2.id})
			print('buy dataaaaa' + str(buy_data))
			res = self.client.get(self.url, buy_data)
			self.assertIn('انجام شد', res.body)

			self.assertEquals(self.customer_2.credit, buyer_credit - product.price)
			self.assertEquals(self.customer.credit, seller_credit + product.price)
			self.assertEquals(product.quantity, 1)

			baught_products = ShopRecord.objects.filter(product=product, remained=1, buyer=self.customer_2, price=15)
			self.assertTrue(baught_products.exists())

			transactions = Transaction.objects.filter(amount=15, source_user=self.customer_2, dest_user=self.customer,
													  type='خرید')
			self.assertTrue(transactions.exists())
