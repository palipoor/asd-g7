from django.contrib import admin

# Register your models here.
from apps.shopping.models import Transaction, ShopRecord

admin.site.register(ShopRecord)