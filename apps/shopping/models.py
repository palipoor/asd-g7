from django.contrib.postgres.fields import IntegerRangeField
from django.core.exceptions import ValidationError
from django.db import models

# Create your models here.
from django_jalali.db import models as jmodels

transaction_types = (
	('خرید', 'خرید'),
	('عودت', 'عودت'),
	('شارژ', 'شارژ'),
	('برداشت', 'برداشت'),
	('ودیعه', 'ودیعه'),
	('بازگشت ودیعه', 'بازگشت ودیعه'),
	('جریمهٔ انصراف', 'جریمهٔ انصراف'),
	('بازگشت جریمهٔ انصراف', 'جریمهٔ انصراف'),
)


class Transaction(models.Model):
	class Meta:
		verbose_name = 'تراکنش'
		verbose_name_plural = 'تراکنش‌ها'

	amount = models.IntegerField(verbose_name='میزان')
	source_user = models.ForeignKey('registeration.Customer', related_name='Transaction_source_user',
									verbose_name='مشتری مبدأ',
									on_delete=models.PROTECT, null=True, blank=True)
	dest_user = models.ForeignKey('registeration.Customer', related_name='Transaction_dest_user',
								  verbose_name='مشتری مقصد',
								  on_delete=models.PROTECT, null=True, blank=True)
	type = models.CharField(choices=transaction_types, max_length=20)

	def clean(self):
		if self.source_user:
			if self.source_user.credit < self.amount:
				raise ValidationError(message='موجودی کیف پول شما کم‌تر از مبلغ این تراکنش است.')

	def get_reverse_transaction(self):
		type = None
		if self.type == 'خرید':
			type = 'عودت'
		elif self.type == 'عودت':
			type = 'خرید'
		elif self.type == 'شارژ':
			type = 'برداشت'
		elif self.type == 'برداشت':
			type = 'شارژ'
		elif self.type == 'ودیعه':
			type = 'بازگشت ودیعه'
		elif self.type == 'بازگشت ودیعه':
			type = 'ودیعه'
		elif self.type == 'جریمهٔ انصراف':
			type = 'بازگشت جریمهٔ انصراف'
		elif self.type == 'بازگشت جریمهٔ انصراف':
			type = 'جریمهٔ انصراف'
		return Transaction(source_user=self.dest_user, dest_user=self.source_user, amount=self.amount, type=type)

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		has_source = False
		has_dest = False
		try:
			if self.source_user:
				has_source = True
		except:
			pass
		try:
			if self.dest_user:
				has_dest = True
		except:
			pass
		if self.type == None or self.type == "":
			if has_source and has_dest:
				self.type = 'خرید'
			elif has_source:
				self.type = 'برداشت'
			else:
				self.type = 'شارژ'
		super().save(force_insert, force_update, using, update_fields)

	def apply(self):
		if self.dest_user and self.source_user:
			self.dest_user.credit += self.amount
			self.dest_user.save()
			self.source_user.credit -= self.amount
			self.source_user.save()
		elif self.dest_user:
			self.dest_user.credit += self.amount
			self.dest_user.save()
		elif self.source_user:
			self.source_user.credit -= self.amount
			self.source_user.save()


class ShopRecord(models.Model):
	class Meta:
		verbose_name = 'رکورد خرید'
		verbose_name_plural = 'رکوردهای خرید'

	buyer = models.ForeignKey('registeration.Customer', verbose_name='خریدار ', on_delete=models.PROTECT)
	product = models.ForeignKey('product.Product', verbose_name='محصول خریداری شده', on_delete=models.PROTECT)
	quantity = models.IntegerField(verbose_name='تعداد خریداری شده')
	price = models.IntegerField(verbose_name='قیمت')
	time = jmodels.jDateTimeField(verbose_name='زمان خرید', auto_now_add=True, blank=True)

	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		self.product.remaining -= self.quantity
		self.product.save()

		super().save(force_insert, force_update, using, update_fields)
