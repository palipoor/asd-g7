from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ValidationError
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from apps.product.models import Product
from apps.registeration.models import Customer
from apps.shopping.models import Transaction, ShopRecord


class BuyProductView(LoginRequiredMixin, TemplateView):
    template_name = 'shopping/buy.html'

    def error_response(self, request, message, **kwargs):
        print(' sending erorr response! sorry')
        context = self.get_context_data(**kwargs)
        context['message'] = message
        context['is_error'] = True
        return render(request, self.template_name, context)

    def success_response(self, request, message, **kwargs):
        print(' sending success response! ')
        context = self.get_context_data(**kwargs)
        context['message'] = message
        context['is_error'] = False
        return render(request, self.template_name, context)

    def get(self, request, *args, **kwargs):
        print(kwargs)
        buyer_id = request.GET.get('buyer', '')
        if buyer_id == '':
            print('2')
            return render(request, self.template_name, self.get_context_data(**kwargs))
        print('3')
        seller_id = int(request.GET['seller'])
        prodcut_id = int(request.GET['prod'])
        quantity = int(request.GET['quantity'])

        buyer = Customer.objects.filter(id=buyer_id)[0]
        seller = Customer.objects.filter(id=seller_id)[0]
        product = Product.objects.filter(id=prodcut_id)[0]
        if not buyer or not seller:
            return self.error_response(request, 'اطلاعات فروشنده و خریدار نامعتبر است. متاسفانه خرید شما انجام نگرفت.',
                                       **kwargs)
        if not product:
            return self.error_response(request, 'شناسهٔ محصول نامعتبر است. متاسفانه خرید شما انجام نگرفت.', **kwargs)
        if quantity > product.remaining:
            return self.error_response(request,
                                       'تعداد درخواستی شما بیشتر از تعداد باقی‌مانده از این محصول است. متاسفانه خرید شما انجام نگرفت.',
                                       **kwargs)
        overall_price = quantity * product.price

        transaction = Transaction(source_user=buyer, dest_user=seller, amount=overall_price)

        try:
            transaction.clean()
        except ValidationError as ve:
            return self.error_response(request, ve.message, **kwargs)
        else:
            transaction.save()
            transaction.apply()
            shop_record = ShopRecord(buyer=buyer, product=product, price=overall_price, quantity=quantity)
            shop_record.save()
            print('kwargs is ' + str(kwargs))
            return self.success_response(request, 'شما' + str(
                quantity) + 'عدد از محصول' + product.name + 'را به قیمت هر یک عدد' + str(
                product.price) + 'خریداری کردید.', **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if 'prod' in kwargs:
            context['product'] = Product.objects.filter(id=kwargs['prod'])[0]
            context['seller'] = Customer.objects.filter(id=kwargs['seller'])[0]
            context['range'] = range(1, context['product'].remaining + 1)

        return context
