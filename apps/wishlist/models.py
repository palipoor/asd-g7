from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models

# Create your models here.
from taggit.managers import TaggableManager

from apps.location.models import District, City, State
from apps.product.models import Category


class Wishlist(models.Model):
	text = models.CharField("متن جست و جو", max_length=1000, null=True, blank=True)
	district = models.ForeignKey(District, verbose_name="محله", on_delete=models.CASCADE, null=True,
								 blank=True)
	city = models.ForeignKey(City, verbose_name="شهر", on_delete=models.CASCADE, null=True, blank=True)
	state = models.ForeignKey(State, verbose_name="استان", on_delete=models.CASCADE, null=True, blank=True)
	category = models.ForeignKey(Category, verbose_name="دسته", on_delete=models.CASCADE, null=True,
								 blank=True)
	tags = TaggableManager(verbose_name='برچسب‌ها', blank=True, help_text='لیستی از تگ‌ها که با , جدا شده اند.')
	user = models.ForeignKey(User, verbose_name="کاربر", on_delete=models.CASCADE)
	min_price = models.IntegerField("قیمت کمینه",
									validators=[MinValueValidator(0, message="مقدار داده شده باید مثبت باشد.")],
									null=True, blank=True)
	max_price = models.IntegerField("قیمت بیشینه",
									validators=[MinValueValidator(0, message="مقدار داده شده باید مثبت باشد.")],
									null=True, blank=True)