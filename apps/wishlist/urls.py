from django.conf.urls import url
from django.urls import path

from apps.wishlist.views import WishlistListView, CreateWishlistView

urlpatterns = [
	path('create_wish/', CreateWishlistView.as_view(), name='create-wishlist'),
	path('wishlist-list/', WishlistListView.as_view(), name='wishlist-list'),

]
