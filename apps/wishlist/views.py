from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView

from apps.wishlist.models import Wishlist


class WishlistListView(LoginRequiredMixin, ListView):
	template_name = 'wishlist/wishlist_list.html'
	model = Wishlist

	def get_queryset(self):
		user = self.request.user
		wishlists = Wishlist.objects.filter(user=user)
		return wishlists


class CreateWishlistView(CreateView):
	model = Wishlist
	success_url = reverse_lazy('dashboard:wishlist')
	template_name = 'wishlist/create_whishlist.html'
	fields = [
		"text",
		"state",
		"city",
		"district",
		"category",
		"tags",
		"min_price",
		"max_price"
	]

	def form_valid(self, form):
		user = self.request.user
		print("user is ", user.username)
		form.instance.user = user
		return super(CreateWishlistView, self).form_valid(form)
