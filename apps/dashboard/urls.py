from django.conf.urls import url
from django.urls import path

from apps.dashboard.views import *

urlpatterns = [
	path('dashboard', profile, name='dashboard'),
	url('profile/(?P<username>[0-9A-Za-z_\-]+)', profile_view, name='profile'),
	url('mytransactions/', my_transactions, name='transactions'),
	url('boughtproducts/', bought_products, name='bought_products'),
	url('myproducts/', my_products, name='my_products'),
	url('wishlist/', my_wishlist, name='wishlist'),
	url('wallet/', wallet, name='wallet'),
	url('myadvs',my_advs, name='myadvs'),
	url('bank/success', bank_success, name='bank-success'),
	url('bank/failure', bank_failure, name='bank-failure')
]
