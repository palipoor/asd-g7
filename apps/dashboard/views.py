import json

import requests
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect

from apps.advertisement.models import Advertisement
from apps.location.models import Location
from apps.product.models import *
from apps.registeration.models import *
from apps.shopping.models import Transaction, ShopRecord
from apps.wishlist.models import Wishlist

bank_url = 'https://bank-asd.herokuapp.com'
# bank_url = 'http://127.0.0.1:8787'


def get_advertise_context():
	try:
		adv1s = Advertisement.objects.filter(place="بالا", page="پنل کاربر", is_active=True)
		r = random.randint(0, len(adv1s) - 1)
		adv1 = adv1s[r]
	except:
		adv1 = None
	try:
		adv2s = Advertisement.objects.filter(place="پایین", page="پنل کاربر", is_active=True)
		r = random.randint(0, len(adv2s) - 1)
		adv2 = adv2s[r]
	except:
		adv2 = None

	return {'adv1': adv1, 'adv2': adv2}


def bank_success(request):
	try:
		token = request.GET['token']
		user_id_and_amount = token[36:]
		user_id = int(user_id_and_amount.split('--')[0])
		amount = int(user_id_and_amount.split('--')[1])
		customer = Customer.objects.get(id=user_id)
		transaction = Transaction(dest_user=customer, amount=amount)
		transaction.save()
		transaction = Transaction.objects.get(pk=transaction.pk)
		transaction.apply()
		return render(request, 'registeration/payment.html', {'amount': amount, 'user': customer})
	except error as e:
		return error(request, 'مشکلی در ارتباط با بانک پیش آمده.')


def bank_failure(request):
	return error(request, 'مشکلی در پرداخت شما بوده. ')


def current_site_url(port=''):
	from django.contrib.sites.models import Site
	current_site = Site.objects.get_current()
	print('current site ' + current_site.domain)
	print(current_site.__dict__)
	protocol = 'http'
	url = '%s://%s' % (protocol, current_site.domain)
	if port:
		url += ':%s' % port
	return url


@login_required
def wallet(request):
	if 'credit-input' in request.POST:
		print('here here charging')
		amount = int(request.POST['credit-input'])
		if amount > 0:
			url = request.build_absolute_uri().replace('/wallet', '/bank')
			print('url is ' + url)
			try:
				resp = requests.post(bank_url + '/getToken',
									 data={'amount': amount, 'url': url, 'dest': 'سخوف', 'user': request.user.id},
									 timeout=5)
				if resp.status_code != 200:
					return error(request, "متاسفانه پاسخ مناسبی از درگاه پرداخت دریافت نشد.")

				body = json.loads(resp.text)
				token = body.get('token')
				url = body.get('url')
				if not token or not url:
					return error(request, "متاسفانه پاسخ مناسبی از درگاه پرداخت دریافت نشد.")
				else:
					return redirect(to=bank_url + '/Pay?token=' + token)
			except:
				return error(request, "متاسفانه پاسخ مناسبی از درگاه پرداخت دریافت نشد.")

	user = Customer.objects.get(username=request.user.username)
	return render(request, 'dashboard/wallet.html', context={'user': user})


@login_required
def bought_products(request):
	try:
		user = Customer.objects.get(username=request.user.username)
		bought_products = reversed(ShopRecord.objects.filter(buyer=user))
	except:
		bought_products = []

	context = get_advertise_context()
	context.update({'products': bought_products})
	return render(request, 'dashboard/bought_products.html', context=context)


@login_required
def my_transactions(request):
	try:
		user = Customer.objects.get(username=request.user.username)
		transactions = reversed(Transaction.objects.filter(Q(dest_user=user) | Q(source_user=user)))
	except:
		transactions = []
	context = get_advertise_context()
	context.update({'requests': transactions})

	return render(request, 'dashboard/transactions.html', context=context)


@login_required
def my_advs(request):
	try:
		user = Customer.objects.get(username=request.user.username)
		advertisements = Advertisement.objects.filter(product__creator=user, is_active=True)
	except:
		advertisements = []

	print(advertisements)
	context = get_advertise_context()
	context.update({'advertisements': advertisements})

	return render(request, 'dashboard/my_advs.html', context=context)


@login_required
def my_products(request):
	try:
		user = Customer.objects.get(username=request.user.username)
		products = reversed(Product.objects.filter(creator=user))
	except:
		products = []
	context = get_advertise_context()
	advertisements = Advertisement.objects.filter(product__creator=request.user, is_active=True)
	context.update({'ps': products, 'advertisements': advertisements})
	return render(request, 'dashboard/my_products.html', context=context)


@login_required
def my_wishlist(request):
	try:
		user = Customer.objects.get(username=request.user.username)

		wish_products = reversed(Wishlist.objects.filter(user=user).reverse())
	except:
		wish_products = []
	context = get_advertise_context()
	context.update({'wishlist': wish_products})
	return render(request, 'dashboard/my_wishlist.html', context=context)


def profile(request):
	try:
		user = Customer.objects.get(username=request.user.username)
	except:
		return error(request, "شما در قالب اکانت مشتری وارد سایت نشده اید.")

	try:
		locations = [str(l) for l in Location.objects.filter(customer_id=user.id)]
	except:

		locations = []
	context = get_advertise_context()
	context.update({'user': user, 'locations': locations})
	return render(request, 'dashboard/dashboard.html', context=context)

@login_required
def profile_view(request, username):
	try:
		user = Customer.objects.get(username=username)
		return render(request, 'dashboard/profile.html', {'user': user})

	except:
		return error(request, "کاربری با نام کاربری " + username + " وجود ندارد.")


def error(request, errormsg):
	return render(request, 'dashboard/error.html', {'error': errormsg})
