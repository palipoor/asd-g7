from django.contrib import admin

# Register your models here.
from apps.shopping.models import Transaction

admin.site.register(Transaction)
