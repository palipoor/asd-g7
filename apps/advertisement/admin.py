from django.contrib import admin

# Register your models here.
from apps.advertisement.models import Advertisement

admin.site.register(Advertisement)
