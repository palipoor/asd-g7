from django.conf.urls import url
from django.urls import path

from apps.advertisement.views import CreateAdvertisement
from apps.wishlist.views import WishlistListView, CreateWishlistView

urlpatterns = [
    path('create_advertisement/<int:product_id>/', CreateAdvertisement.as_view(), name='create_advertisement'),
]
