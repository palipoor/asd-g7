import datetime

import jdatetime
import pytz
from django.db import models
from django.db.models import DO_NOTHING
from django_jalali.db import models as jmodels
from dateutil import relativedelta

from apps.product.models import Product
from backend_settings.scheduler import project_scheduler

available_pages = (('صفحه اصلی', 'صفحه اصلی'), ('پنل کاربر', 'پنل کاربر'))
places = (('بالا', 'بالا'), ('پایین', 'پایین'))


class Advertisement(models.Model):
    page = models.CharField(verbose_name='صفحه', choices=available_pages, max_length=20, null=True)
    place = models.CharField(verbose_name='مکان', choices=places, max_length=20, null=True)
    product = models.ForeignKey(Product, on_delete=DO_NOTHING, related_name='product')
    period = models.IntegerField(verbose_name="تعداد روز", default=0)
    hours = models.IntegerField(verbose_name="تعداد ساعت", default=0)
    minutes = models.IntegerField(verbose_name="تعداد دقیقه", default=0)
    date = jmodels.jDateTimeField(verbose_name="زمان شروع تبلیغ")
    is_active = models.BooleanField(verbose_name="در حال اجرا",default=False)
    end_date = jmodels.jDateTimeField(verbose_name="زمان پایان تبلیغ")

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        advs = Advertisement.objects.filter(page=self.page, place=self.place)
        flag = self.id
        if not self.id:
            print("*************************")
            for i in advs:
                print(i.__dict__)
            print("*************************88")
            if len(advs) > 0:
                self.date = advs.latest('end_date').end_date
                if self.date < jdatetime.datetime.now():
                    self.date = jdatetime.datetime.now()
                print("in len is bigger")
                print(self.date)
            else:
                print("in else")
                print(jdatetime.datetime.now())
                self.date = jdatetime.datetime.now()
                print(self.date)
            self.end_date = self.date + jdatetime.timedelta(days=self.period,
                                        hours=self.hours,
                                        minutes=self.minutes)
            print("end date is")
            print(self.date)
            print(self.end_date)
        temp = super().save(force_insert=force_insert, force_update=force_update, using=using,
                            update_fields=update_fields)
        print("THEy are going to save meeee")
        print(self.date)
        print(self.end_date)
        if not flag:     
            project_scheduler.add_start_advertise(self)
            project_scheduler.add_end_advertise(self)
        return temp
