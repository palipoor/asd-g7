import jdatetime
from django.test import TestCase, Client
from django.urls import reverse_lazy

from apps.advertisement.models import Advertisement
from apps.product.models import Product
from apps.shopping.models import Transaction
from apps.location.models import State, City, District
from apps.registeration.models import Customer
import time
from jdatetime import timedelta


def create_customer(username, password, amount=100000):
    customer = Customer(username=username, credit=amount, email_verified=True)
    customer.set_password(password)
    customer.save()
    customer = Customer.objects.get(pk=customer.pk)
    return customer

def create_product(owner, quantity=1, price=1):
    product_name = 'prod' + str(time.time())
    product = Product(name=product_name, description=50*'blah blah', quantity=quantity, creator=owner, price=price,
                      city=City.objects.get(id=1),
                      state=State.objects.get(id=1), district=District.objects.get(id=1))
    product.save()
    product = Product.objects.get(id = product.id)
    return product


def create_advertisement(product, days=1, hours=1, minutes=1, start_time=jdatetime.datetime.now(),
                         page="صفحه اصلی", place="بالا"):
    advertisement = Advertisement(product=product, period=days, hours=hours, minutes=minutes, date=start_time
                                  , page=page, place=place)
    advertisement.save()
    print(advertisement)
    print(advertisement.__dict__)
    return Advertisement.objects.get(id = advertisement.id)


class AdvertisementTest(TestCase):

    def setUp(self):
        self.customer = create_customer('fake_username', 'fake_password', amount=2*(1440+60+1)+1)
        self.username = 'fake_username'
        self.password = 'fake_password'
        self.product = create_product(self.customer)
        self.data = {"place": "پایین", "page": "پنل کاربر", "hours": 1,
                     "minutes": 1, "period": 1
                     }
        self.client.login(username=self.username, password=self.password)


    def tearDown(self):
        self.client.logout()
        return super().tearDown()

    def test_advertisement_money_amount(self):
        customer_credit = self.customer.credit
        response = self.client.post(reverse_lazy('advertisement:create_advertisement',
                                                 kwargs={'product_id': self.product.id}),
                                    self.data, follow=True)
        self.customer = Customer.objects.get(pk=self.customer.pk)        
        self.assertEquals(self.customer.credit, customer_credit - (1440+60+1))

    def test_multiple_advertisements(self):        
        self.client.post(reverse_lazy('advertisement:create_advertisement',
                                      kwargs={'product_id': self.product.id}),
                         self.data, follow=True)
        product = create_product(self.customer)
        response = self.client.post(reverse_lazy('advertisement:create_advertisement',
                                      kwargs={'product_id': product.id}),
                         self.data, follow=True)
        advertisement3 = Advertisement.objects.filter(product=self.product)[0]
        advertisement4 = Advertisement.objects.filter(product=product)[0]
        self.assertAlmostEqual(advertisement4.date, advertisement3.date + jdatetime.timedelta(days=advertisement3.period,hours=advertisement3.hours,minutes=advertisement3.minutes), delta=timedelta(seconds=10))

    def test_create_advertisement_view_is_implemented(self):
        try:
            from apps.advertisement.views import CreateAdvertisement
        except:
            self.fail()

    def test_create_transaction(self):
        response = self.client.post(reverse_lazy('advertisement:create_advertisement',
                                                 kwargs={'product_id': self.product.id}),
                                    self.data, follow=True)
        transactions = Transaction.objects.filter(source_user=self.customer, dest_user=None,
                                                  type='برداشت')
        if len(transactions) == 0:
            self.fail()
        self.assertEquals(transactions[0].amount, (1440+60+1))

    def test_not_enough_money(self):
        self.customer.credit -= 12000
        self.customer.save()
        response = self.client.post(reverse_lazy('advertisement:create_advertisement',
                                                 kwargs={'product_id': self.product.id}),
                                    self.data, follow=True)
        self.assertContains(response, "به پنل کاربری بروید و حساب خود را شارژ کنید.")

    def test_advertise_others_product(self):
        temp_customer = create_customer('fake_username1', 'fake_password1')
        temp_product = create_product(temp_customer)
        response = self.client.post(reverse_lazy('advertisement:create_advertisement',
                                                 kwargs={'product_id': temp_product.id}),
                                    self.data, follow=True)
        self.assertContains(response, "این کالا به شما اختصاص ندارد")
