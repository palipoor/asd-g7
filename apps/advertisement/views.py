from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView
from apps.dashboard.views import *
from apps.product.models import Product
from apps.shopping.models import Transaction
from django import http

class CreateAdvertisement(LoginRequiredMixin, CreateView):
    model = Advertisement
    template_name = "advertisement/advertisement.html"
    success_url = reverse_lazy("dashboard:dashboard")

    fields = ['page', 'place', 'period', 'hours', 'minutes']

    def dispatch(self, request, *args, **kwargs):
        self.product = Product.objects.get(id=kwargs['product_id'])
        if self.product.creator.username != self.request.user.username:
            return error(request, "این کالا به شما اختصاص ندارد")

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.product = self.product
        print("hihihihihihih")
        user = self.request.user
        sum = 0
        if form.instance.page == 'صفحه اصلی':
            sum += 2
        if form.instance.page == 'پنل کاربر ':
            sum += 1
        if form.instance.place == 'بالا':
            sum += 2
        if form.instance.place == 'پایین':
            sum += 1
        sum *= 1440*int(form.instance.period) + 60*int(form.instance.hours) + int(form.instance.minutes)
        try:
            customer = Customer.objects.get(username=user.username)
        except:
            return error(self.request, "شما مشتری نیستید")
        if customer.credit < sum:
            return error(self.request, "هزینه این تبلیغ " + str(sum) + "تومان است در حالی که موجودی شما، " +
                         str(customer.credit)
                         + "است. به پنل کاربری بروید و حساب خود را شارژ کنید.")
        else:
            transaction = Transaction(source_user=customer, amount=sum, dest_user=None)
            transaction.clean()
            transaction.apply()
            transaction.save()
        return super().form_valid(form)
