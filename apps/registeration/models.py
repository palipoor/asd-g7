import random
import string

from django.apps import apps
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse_lazy

from apps.location.models import State, City, District
from utilities.notification_tools import send_notification, notify


class Customer(User):
	email_verified = models.BooleanField(default=False, verbose_name="ایمیل تایید شده است")
	credit = models.IntegerField(default=0, verbose_name="اعتبار")
	referee = models.CharField(max_length=20, verbose_name='معرف', null=True)

	@property
	def full_name(self):
		return self.first_name + ' ' + self.last_name

	@property
	def url(self):
		return reverse_lazy('dashboard:profile', kwargs={"username": self.username})

