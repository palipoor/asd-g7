from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse_lazy
from apps.registeration.forms import ChangePasswordForm
from django.contrib.auth.tokens import default_token_generator
from apps.registeration.tests.registration_helper import Registration_helper
from utilities.client_helpers import get_response_status


class ChangePasswordTest(TestCase):

	def setUp(self):
		self.user, self.password = Registration_helper.create_new_user()
		self.user.save()
		self.token = default_token_generator.make_token(self.user)
		self.client.login(username=self.user.username, password=self.password)

	def tearDown(self):
		User.objects.get(username=self.user.username).delete()

	def test_check_url(self):
		self.assertEquals(get_response_status("change_password", self.client, 'registeration'), 200)

	def test_check_inequal_input(self):
		newpass1 = "asdqwdqwdasd"
		newpass2 = "asdqsfv131"
		incorrect_form = ChangePasswordForm(self.user, {"old_password": self.password, "new_password1": newpass1,
		                                                "new_password2": newpass2})
		self.assertFalse(incorrect_form.is_valid())

	def test_check_equal_input(self):
		newpass1 = "asdqwdqwdasdasdqwdqwdasd"
		newpass2 = newpass1
		correct_form = ChangePasswordForm(self.user, {"old_password": self.password, "new_password1": newpass1,
		                                              "new_password2": newpass2})
		self.assertTrue(correct_form.is_valid())
		changed_user = correct_form.save()
		self.assertTrue(self.user.check_password(newpass1))

	def test_view(self):
		newpass1 = "asdqwdqwdasdasdqwdqwdasd"
		newpass2 = newpass1
		response = self.client.post(reverse_lazy('registeration:change_password'),
		                            {"old_password": self.password, "new_password1": newpass1,
		                             "new_password2": newpass2}, follow=True)
		self.assertTrue(User.objects.get(username=self.user.username).check_password(newpass1))
