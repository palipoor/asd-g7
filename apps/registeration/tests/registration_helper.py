from django.contrib.auth.models import User
from apps.product.models import District
from apps.location.models import State, City
from apps.registeration.models import Customer


class Registration_helper():
	user_count = 0

	@staticmethod
	def create_new_user():
		while (True):
			username = "u_" + str(Registration_helper.user_count)
			if len(User.objects.filter(username=username)) == 0:
				break
			Registration_helper.user_count += 1
		new_user = Customer(username=username, email=username + "@gmail.com")
		password = Customer.objects.make_random_password()
		new_user.set_password(password)
		new_user.email_verified = True
		return new_user, password
