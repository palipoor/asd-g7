from django.test import TestCase
from django.urls import reverse_lazy

from apps.registeration.forms import SignUpForm
from apps.registeration.models import Customer
from apps.registeration.tests.registration_helper import Registration_helper
from utilities.client_helpers import get_response_status


class RegisterTest(TestCase):

	def setUp(self):
		self.user, self.password = Registration_helper.create_new_user()
		self.username = self.user.username
		self.data = {"username": self.username, "email": self.username + "@gmail.com", "password1": self.password,
					 "password2": self.password, "first_name": 'یک نام', 'last_name': 'یک فامیل', 'city': '8',
					 'state': '13', 'district': '9'}

	def test_check_url(self):
		self.assertEquals(get_response_status("register", self.client, 'registeration'), 200)

	def test_valid_register_form(self):
		correct_form = SignUpForm(data=self.data)
		self.assertTrue(correct_form.is_valid())
		correct_form.save()
		Customer.objects.get(username=self.username)

	def test_different_pass_register_form(self):
		self.user.save()
		self.data["password2"] = self.data["password1"] + "somethingelse"
		correct_form = SignUpForm(data=self.data)
		self.assertFalse(correct_form.is_valid())


	def test_duplicate_user_unsuccessful_view(self):
		self.user.save()
		self.password = self.password + "somethingelse"
		self.data["password1"] = self.password
		self.data["password2"] = self.password
		response = self.client.post(reverse_lazy('registeration:register'), self.data, follow=True)
		self.assertContains(response, "کاربری با آن نام کاربری وجود دارد.")
