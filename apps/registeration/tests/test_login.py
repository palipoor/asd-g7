from django.test import TestCase
from django.urls import reverse_lazy
from django.contrib import auth
from apps.registeration.tests.registration_helper import Registration_helper
from utilities.client_helpers import get_response_status


class LoginTest(TestCase):

	def test_login(self):
		self.user, self.password = Registration_helper.create_new_user()
		self.user.save()
		self.username = self.user.username
		response = self.client.post(reverse_lazy('registeration:login'), {"username": self.username, "password": self.password},
		                            follow=True)
		self.assertIn('_auth_user_id', self.client.session)

	def test_invalid_login(self):
		self.user, self.password = Registration_helper.create_new_user()
		self.username = self.user.username
		self.client.post(reverse_lazy('registeration:login'), {"username": self.username, "password": self.password},
		                 follow=True)
		auth.get_user(self.client)
		self.assertNotIn('_auth_user_id', self.client.session)

	def test_check_url(self):
		self.assertEquals(get_response_status("login", self.client, 'registeration'), 200)
