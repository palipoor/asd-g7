from django.urls import reverse_lazy

from apps.registeration.tests.registration_helper import Registration_helper
from django.test import TestCase


class LogoutTest(TestCase):

	def setUp(self):
		self.user, self.password = Registration_helper.create_new_user()
		self.user.save()
		self.client.login(username=self.user.username, password=self.password)

	def test_logout(self):
		self.client.get(reverse_lazy('registeration:logout'))
		self.assertNotIn('_auth_user_id', self.client.session)
