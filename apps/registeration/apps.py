from django.apps import AppConfig


class RegisterationConfig(AppConfig):
    name = 'apps.registeration'
    verbose_name='افراد'
