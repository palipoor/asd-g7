from math import ceil

from django import forms
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.forms import ModelForm
from utilities.notification_tools import send_email
from apps.location.models import Location
from apps.product.models import *
from apps.registeration.models import Customer
from django.contrib.auth.forms import UserCreationForm, SetPasswordForm
from django.contrib.auth.forms import PasswordChangeForm as DjangoPasswordChangeForm

from backend_settings import settings


class ChangePasswordForm(DjangoPasswordChangeForm):
	def __init__(self, user, *args, **kwargs):
		super().__init__(user, *args, **kwargs)
		self.fields['old_password'].label = "رمز عبور"
		self.fields['new_password1'].label = "رمز عبور جدید"
		self.fields['new_password2'].label = "تکرار رمز عبور جدید"


class SignUpForm(UserCreationForm):
	first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
	code = forms.CharField(max_length=20, min_length=10, required=False)
	state = forms.CharField(label='استان')
	city = forms.CharField(label='شهر')
	district = forms.CharField(label="محله")

	def __init__(self, *args, **kwargs):
		if 'url' in kwargs:
			self.url = kwargs.pop('url')
		else:
			self.url = '127.0.0.0.1:8000/'

		super(UserCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].label = "ایمیل"
		self.fields['code'].label = "کد معرفی"
		self.fields['username'].label = "نام کاربری"
		self.fields['first_name'].label = "نام"
		self.fields['last_name'].label = "نام خانوادگی"
		self.fields['password1'].label = "رمز عبور"
		self.fields['password2'].label = "تکرار رمز عبور"

	class Meta:
		model = Customer
		fields = (
			'username', 'first_name', 'last_name', 'email', 'password1', 'password2',)

	def clean(self):
		cleaned_data = super().clean()
		if 'city' in cleaned_data and 'state' in cleaned_data and 'district' in cleaned_data:
			print('boood :D :| ')
			city = City.objects.get(id=cleaned_data.get('city'))
			state = State.objects.get(id=cleaned_data.get('state'))
			district = District.objects.get(id=cleaned_data.get('district'))
			if city.id == 0 or state.id == 0 or district.id == 0:
				raise ValidationError(message='لازم است تمامی فیلدهای شهر و استان و محله انتخاب شوند.')
		else:
			raise ValidationError(message='لازم است تمامی فیلدهای شهر و استان و محله انتخاب شوند.')

	def clean_email(self):
		email = self.cleaned_data.get('email')
		username = self.cleaned_data.get('username')
		if email and User.objects.filter(email=email).exclude(username=username).exists():
			raise forms.ValidationError("ایمیل وارد شده تکراری است.")
		return email

	def clean_code(self):
		code = self.cleaned_data.get('code')
		print(' i am cleaning code! ')
		if code:
			try:
				username = code.replace('INVITEBY', '')
				customer = Customer.objects.get(username=username)
				return username
			except:
				raise forms.ValidationError("کد معرفی نامعتبر است.")

	def save(self, commit=True):
		user = super(UserCreationForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password1"])

		if 'code' in self.cleaned_data:
			print(' I am here! setting referee')
			user.referee = self.cleaned_data['code']

		if commit:
			user.save()
			city = City.objects.get(id=self.cleaned_data.get('city'))
			state = State.objects.get(id=self.cleaned_data.get('state'))
			district = District.objects.get(id=self.cleaned_data.get('district'))
			loc = Location(city=city, state=state, district=district, customer=user)
			loc.save()
		return user


class EditProfileForm(ModelForm):
	state = forms.CharField(label='استان')
	city = forms.CharField(label='شهر')
	district = forms.CharField(label="محله")

	class Meta:
		model = Customer
		fields = ('first_name', 'last_name')

	def clean(self):
		cleaned_data = super().clean()
		print('ey baba pas chi mishe! ')
		if 'city' in cleaned_data and 'state' in cleaned_data and 'district' in cleaned_data:
			print('boood :D :| ')
			city = City.objects.get(id=cleaned_data.get('city'))
			state = State.objects.get(id=cleaned_data.get('state'))
			district = District.objects.get(id=cleaned_data.get('district'))
			if city.id == 0 or state.id == 0 or district.id == 0:
				raise ValidationError(message='لازم است تمامی فیلدهای شهر و استان و محله انتخاب شوند.')
			loc = Location(city=city, state=state, district=district, customer=self.instance)
			loc.save()
		else:
			raise ValidationError(message='لازم است تمامی فیلدهای شهر و استان و محله انتخاب شوند.')


class ForgetPassForm(forms.Form):
	username = forms.CharField(max_length=20, required=True, label="نام کاربری")

	class Meta:
		model = Customer
		fields = 'username'



class ResetPasswordForm(SetPasswordForm):
	def __init__(self, user, *args, **kwargs):
		super().__init__(user, *args, **kwargs)
		self.fields['new_password1'].label = "رمز عبور جدید"
		self.fields['new_password2'].label = "تکرار رمز عبور جدید"


class Hash:
	@staticmethod
	def dohash(name):
		sums = [0 for _ in range(ceil(len(name) / 4))]
		for j in range(ceil(len(name) / 4)):
			for i in range(4):
				if 4 * j + i < len(name):
					sums[j] = sums[j] * 256 + ord(name[4 * j + i])
		res = ''
		for j in range(ceil(len(name) / 4)):
			current_sum = sums[j]
			while current_sum > 25:
				a = current_sum % 26
				current_sum = (current_sum - a) / 26
				res = chr(int(97 + a)) + res
			res = chr(int(65 + current_sum)) + res
		return res

	@staticmethod
	def unhash(hashed):
		i = 0
		dehashed = ''
		while i < len(hashed):
			sum_result = ord(hashed[i]) - 65
			i += 1
			while i < len(hashed) and ord(hashed[i]) >= 97:
				a = ord(hashed[i]) - 97
				sum_result = sum_result * 26 + a
				i += 1
			while sum_result > 255:
				a = sum_result % 256
				sum_result = (sum_result - a) / 256
				dehashed = chr(int(a)) + dehashed
			dehashed = chr(int(sum_result)) + dehashed
		return dehashed
