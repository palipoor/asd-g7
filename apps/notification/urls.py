from django.conf.urls import url
from django.urls import path

from apps.notification.views import NotificationsView, NotificationDetailsView

urlpatterns = [
	path('notifications/', NotificationsView.as_view(), name='notifications'),
	path('notification-details/<int:pk>', NotificationDetailsView.as_view(), name='notification-details'),

]
