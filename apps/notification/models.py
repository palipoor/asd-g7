from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django_jalali.db import models as jmodels


class Notification(models.Model):
	sent_date = jmodels.jDateTimeField(auto_now_add=True, verbose_name="زمان ارسال")
	seen = models.BooleanField(default=False, verbose_name="دیده شده")
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Notification_receiver')
	title = models.CharField(max_length=100, verbose_name="عنوان")
	text = models.TextField(max_length=1000, verbose_name="متن")
	sender_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Notification_sender', null=True)

	@staticmethod
	def create(title, message, dest_username, source_username=None):
		dest_user = User.objects.get(username=dest_username)
		temp = Notification(user=dest_user, title=title, text=message)
		temp.seen = False
		if source_username:
			source_user = User.objects.get(username=source_username)
			temp.sender_user = source_user
		temp.save()
		return temp

	@property
	def url(self):
		# TODO use revers
		# return reverse_lazy("registeration:notification-details",self.id)
		return "/notification-details/" + str(self.id)