from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, CreateView

from apps.notification.models import Notification


class NotificationsView(LoginRequiredMixin, ListView):
	template_name = 'notification/notifications.html'
	model = Notification

	def get_queryset(self):
		qs = Notification.objects.filter(Q(user=self.request.user) | Q(sender_user=self.request.user))
		for q in qs:
			q.seen = True
			q.save()
		return qs


class NotificationDetailsView(CreateView):
	model = Notification
	template_name = "notification/notification-details.html"
	fields = [
		"title",
		"text",
	]

	def get_context_data(self, **kwargs):
		data = super().get_context_data(**kwargs)
		data["message"] = Notification.objects.get(id=self.kwargs["pk"])
		return data

	def get_success_url(self):
		# TODO use reverse
		# return reverse_lazy("product:product-detail",self.kwargs["pk"])
		return str(self.kwargs["pk"])

	def form_valid(self, form):
		source_user = self.request.user
		form.instance.sender_user = source_user
		dest_user = Notification.objects.get(id=self.kwargs["pk"]).sender_user
		form.instance.user = dest_user
		form.instance.seen = False
		return super().form_valid(form)
