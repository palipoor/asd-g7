import pkg_resources as resources


def get_resource_address(resource_name, resource_package='resources'):
	"""
	Get absolute address for `resource_name`
	:param str resource_name:
	:param str resource_package:
	:return: absolute path for accessing the resource file inside resources package
	"""
	filepath = resources.resource_filename(resource_package, resource_name)
	return filepath


products = [
	{
		"name": "خانه ی رویایی",
		"creator": "user_0",
		"price": 10000,
		"quantity": 1,
		"description": "زیباترین خانهٔ دنیا که می‌توانید تصور آن را بکنید. سرشار از انرژی‌های خوب",
		"district": None,
		"city": "سرعین",
		"state": "اردبیل",
		"category": "خانه و ویلا",
		"is_trending": True,
		"image": get_resource_address('house.jpg', 'data.images'),
		"product_state": 'کاملا نو',
		"material": "بتن"
		# "tags":,
	},
	{
		"name": "ماشین عروس",
		"creator": "user_1",
		"price": 12000,
		"quantity": 3,
		"description": "ماشینی زیبا و جادار با بهترین بوق برای شبی به یاد ماندنی ....",
		"district": "اوین",
		"city": "تهران",
		"state": "تهران",
		"category": "وسایل نقلیه",
		"is_trending": False,
		"image": get_resource_address('mashin_aroos.jpg', 'data.images'),
		"product_state": 'در حد نو'
		# "tags":,
	},
	{
		"name": "ماشین داماد",
		"creator": "user_4",
		"price": 1200,
		"quantity": 1,
		"description": "بالاخره هر تفاوت قیمتی تفاوت کیفیت رو نشون می‌ده.",
		"district": None,
		"city": "ابریشم",
		"state": "اصفهان",
		"category": "وسایل نقلیه",
		"is_trending": False,
		"image": get_resource_address('mashin_damad.jpg', 'data.images'),
		"product_state": 'کهنه'

		# "tags":,
	},

	{
		"name": "یخچال برفکی",
		"creator": "user_1",
		"price": 12000,
		"quantity": 2,
		"description": "خیلی یخچال خوبیه، من خودم برای خواهرم بردم واقعا راضیه. وقتی که آب رو میذاری توش فرداش که بطری رو در میاری واقعا خنک شده.",
		"district": None,
		"city": "ارجمند",
		"state": "تهران",
		"category": "لوازم خانگی",
		"is_trending": False,
		"image": get_resource_address('yakhchal.jpg', 'data.images'),
		"product_state": 'استفاده شده'
		# "tags":,
	},

	{
		"name": "پیراهن زیبا",
		"creator": "user_2",
		"price": 1200,
		"quantity": 1,
		"description": "این یک پیراهن زیباست که هر کسی بپوشه زیبا می‌شه. به همه هم میاد!",
		"district": "آزادی",
		"city": "تهران",
		"state": "تهران",
		"category": "پوشیدنی‌ها",
		"is_trending": False,
		"image": get_resource_address('dress.jpg', 'data.images'),
		"product_state": 'در حد نو',
		'material': 'نخ صددرصد'
		# "tags":,
	}
]
