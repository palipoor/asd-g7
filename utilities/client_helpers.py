from django.urls import reverse_lazy


def get_response_status(name, client, app_name):
	"""
	:param name: view name
	:param client: client used to get response
	:return: response status code
	"""
	response = client.get(reverse_lazy(app_name + ":"+ name))
	return response.status_code
