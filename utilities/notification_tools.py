from django.apps import apps
from django.core.mail import send_mail
from django.http import BadHeaderError
from django.core.mail import send_mail
from backend_settings import settings


def send_notification(title, message, dest_username, source_username=None):
	return apps.get_model("notification", "Notification").create(title, message, dest_username, source_username)


def send_email(title, message, dest_username, source_username=None):
	dest_user = apps.get_model("auth", "User").objects.get(username=dest_username)
	if source_username:
		message = "از طرف " + source_username + " :\n" + message
	else:
		message = "از طرف " + "سامانه" + " :\n" + message
	try:
		send_mail(
			title,
			message,
			settings.EMAIL_HOST_USER,
			[dest_user.email],
			fail_silently=False,
		)
		print('sent!!', dest_user.email)
		return True
	except BadHeaderError as e:
		print(e)
		return False


def notify(title, message, dest_username, source_username=None):
	send_email(title, message, dest_username, source_username)
	send_notification(title, message, dest_username, source_username)
