
from django.core.exceptions import ValidationError
from django.apps import apps

def validate_existing_category(value):
    if apps.get_model("product","Category").objects.filter(name = value):
        raise ValidationError(
            'کتگوری ' + value + ' در سامانه موجود است.',
        )


def validate_customer_has_credit(customer, value):
    if customer.credit < value:
        raise ValidationError(
            "موجودی حساب شما باید حداقل برابر ۲٪ حداقل قیمت باشد."
        )